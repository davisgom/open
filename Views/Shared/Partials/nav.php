<button class="menu-btn btn btn-light d-md-block d-lg-none d-print-none" onclick="navToggle()" onkeydown="navToggle()">
  <span class="menu-icon">
    <i class="fas fa-bars"></i> <br /> <span>Menu</span>
  </span>
  <span class="search-icon">
    <i class="fas fa-search fa-lg"></i>
  </span>
</button>

<section id="siteNav" class="overlay">
  <div class="overlay-content">
    <div class="container">
      <a class="closebtn d-lg-none" href="javascript:void(0)" onclick="navToggle()"><i class="fa fa-times" aria-hidden="true"></i> CLOSE</a>

      <div id="MSUSearchTool" class="no-fill d-lg-none" role="search">
    			<?php include("msu-search.php"); ?>
    	</div>

      <nav class="row">
        <h1 class="visually-hidden">Site Navigation</h1>
        <ul class="nav nav-pills nav-fill site-nav" role="navigation">
          <li>
            <a class="nav-link" href="home">
              Home
            </a>
          </li>

          <li>
            <a class="nav-link dropdown-toggle" data-bs-auto-close="outside" data-bs-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
              About
            </a>

            <ul class="dropdown-menu">
              <span>
                <li><a class="dropdown-item" href="about">About ACORN</a></li>
                <li><a class="dropdown-item" href="#">ACORN Team</a></li>
                <li><a class="dropdown-item" href="#">Research Network</a></li>
                <li><a class="dropdown-item" href="#">Community Network</a></li>
                <li><a class="dropdown-item" href="#">Advisory Council</a></li>
              </span>
            </ul>
          </li>

          <li>
            <a class="nav-link dropdown-toggle" data-bs-auto-close="outside" data-bs-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
              Programs
            </a>

            <ul class="dropdown-menu">
              <span>
                <li><a class="dropdown-item" href="mini-grants">Mini Grants</a></li>
                <li><a class="dropdown-item" href="clinical-support">Clinical Support</a></li>
                <li><a class="dropdown-item" href="capacity-building">Capacity Building</a></li>
                <li><a class="dropdown-item" href="#">Education</a></li>
              </span>
            </ul>
          </li>

          <li><a class="nav-link" href="#">Resources</a></li>

          <li><a class="nav-link" href="#">Media</a></li>

          <li><a class="nav-link" href="#">Contact</a></li>
        </ul>
      </nav>
    </div>
  </div>

  <div class="container closebtn-bottom d-lg-none">
		<a class="closebtn" href="javascript:void(0)" onclick="navToggle()"><i class="fa fa-times" aria-hidden="true"></i> CLOSE</a>
	</div>
</section>
