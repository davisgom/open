<button class="menu-btn btn btn-light d-md-block d-lg-none d-print-none" onclick="navToggle()" onkeydown="navToggle()">
  <span class="menu-icon">
    <i class="fas fa-bars"></i> <br /> <span>Menu</span>
  </span>
  <span class="search-icon">
    <i class="fas fa-search fa-lg"></i>
  </span>
</button>

<section id="siteNav" class="overlay">
  <div class="overlay-content">
    <div class="container">
      <a class="closebtn d-lg-none" href="javascript:void(0)" onclick="navToggle()"><i class="fa fa-times" aria-hidden="true"></i> CLOSE</a>

      <div id="MSUSearchTool" class="no-fill d-lg-none" role="search">
    			<?php include("msu-search.php"); ?>
    	</div>

      <nav class="row">
        <h1 class="visually-hidden">Site Navigation</h1>
        <ul class="nav nav-pills nav-fill site-nav" role="navigation">
          <li>
            <a class="nav-link" href="home">
              Home
            </a>
          </li>

          <li>
            <a class="nav-link dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
              About
            </a>

            <ul class="dropdown-menu">
              <span>
                <li><a class="dropdown-item" href="#">About ACORN</a></li>
                <li><a class="dropdown-item" href="#">ACORN Team</a></li>
                <li><a class="dropdown-item" href="#">Research Network</a></li>
                <li><a class="dropdown-item" href="#">Community Network</a></li>
                <li><a class="dropdown-item" href="#">Advisory Council</a></li>
              </span>
            </ul>
          </li>

          <li>
            <a class="nav-link dropdown-toggle" data-bs-auto-close="outside" data-bs-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
              Programs
            </a>

            <ul class="dropdown-menu">
              <li>
                <a class="nav-link dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                  Mini Grants
                </a>

                <ul class="dropdown-menu">
                  <li><a class="dropdown-item" href="#">Funded Cohorts</a></li>
                </ul>
              </li>
              
              <li>
                <a class="nav-link dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                  Clinical Support
                </a>

                <ul class="dropdown-menu">
                  <li><a class="dropdown-item" href="#">About ACORN</a></li>
                  <li><a class="dropdown-item" href="#">ACORN Team</a></li>
                  <li><a class="dropdown-item" href="#">Research Network</a></li>
                </ul>
              </li>

              <li>
                <a class="nav-link dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                  Capacity Building
                </a>

                <ul class="dropdown-menu">
                  <li><a class="dropdown-item" href="#">Vista Members in Communities</a></li>
                  <li><a class="dropdown-item" href="#">Community Assessment Support</a></li>
                </ul>
              
              </li>
              
            </ul>
          </li>

          <li>
            <a class="nav-link" href="#">
              Resources
            </a>
          </li>

          <li>
            <a class="nav-link" href="#">
              Media
            </a>
          </li>

          <li>
            <a class="nav-link" href="contact">
              Contact
            </a>
          </li>
        </ul>
      </nav>
    </div>
  </div>

  <div class="container closebtn-bottom d-lg-none">
		<a class="closebtn" href="javascript:void(0)" onclick="navToggle()"><i class="fa fa-times" aria-hidden="true"></i> CLOSE</a>
	</div>
</section>
