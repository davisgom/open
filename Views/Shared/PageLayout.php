<?php ob_start(); include ("Content/Pages/$page_content.php"); $content = ob_get_clean(); ?>

<header class="page-header">
  <div class="container">
    <div class="row">
      <div class="col-11">
        <?php if (isset($page_section)){echo "<aside>". $page_section . "</aside>";}?>
        <h1>
          <?php if (isset($page_title)){echo $page_title;}else{echo $page_content;} ?>
        </h1>
      </div>
    </div>
  </div>
</header>

<section class="container">
  <div class="row">
    <div class="col-11 py-5">
      <?php include ("Content/Pages/$page_content.php"); ?>
    </div>
  </div>
</section>
