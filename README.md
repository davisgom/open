# ACORN Prototype

This is the prototype for the 2024 design of the Addiction Consortium on Research & Education Network (ACORN) website.  

You can view the prototype online at: [https://acorn.mdavis.in](https://acorn.mdavis.in)  
