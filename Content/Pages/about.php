
<?php $page_title = "About Acorn"; ?>

  
<p>
By bringing together MSU faculty, students, volunteers, and staff working on the topic of addiction, the Addiction Consortium on Research & Education Network (ACORN) seeks to change the lives of those impacted by substance use through our collective expertise in community engagement, advocacy, public policy, research, patient care, and teaching.
</p>

<p>
Led by Michigan State University's College of Human Medicine and University Outreach and Engagement with support from the School of Social Work, ACORN serves as a central hub for improving the lives of those impacted by substance use. Its activities are in three communities: MSU, local Michigan, and national. 
</p>

<p>
ACORN's work revolves around the following five pillars: 
</p>

<ol>
  <li><strong>Clinical Support</strong>: leveraging existing collaborations to create an innovative, comprehensive addiction treatment service network in partnership with HFH.</li>
<li><strong>Community Engagement</strong>: Supporting and growing community engagement.</li>
<li><strong>Research</strong>: Centering our research around a health equity framework in addiction science, focusing on community assessments and stakeholder engagement.</li>
<li><strong>Education</strong>: Growing anti-stigma substance use disorder teaching, training, and education and couple these with ongoing research.</li>
<li><strong>Advocacy</strong>: Being a nidus for state and national advocacy to support equitable, evidence-based legislation and policymaking.</li>
</ol>

<h2>
  Potential impact
</h2>

<ul>
  <li>Transform public policy for populations experiencing significant adversities</li>
  <li>Build on our expertise in engagement, clinical care, research, education, and advocacy to produce revolutionary impacts</li>
  <li>Generate our response to the urgent call to action and position MSU as a national leader in multidisciplinary and multipronged transformative care for those most vulnerable individuals and communities experiencing the harms of addiction</li>
</ul>