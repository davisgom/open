<h1>
  <?php echo str_replace("-", " ", ucfirst($page_content)); ?>
</h1>

<p>
  The Opioid Prevention and Education Network (OPEN-MSU) is a program coordinated by Michigan State University's <a href="http://engage.msu.edu/" <?php echo $external;?>>Office of University Outreach and Engagement</a> and  funded by AmeriCorps. The goal of OPEN-MSU is to build the capacity of local efforts to create or expand opioid prevention, intervention, and treatment opportunities in low-income Michigan communities. We do this by providing nonprofit, faith-based, and public organizations and agencies with volunteers who serve in <a href="https://www.nationalservice.gov/programs/americorps/americorps-programs/americorps-vista" <?php echo $external ?>>AmeriCorps VISTA</a> (Volunteers in Service to America). Through this national service program, VISTA members serve with organizations for one year on projects designed to alleviate poverty.
</p>

<p>
  OPEN-MSU supports local efforts to address the opioid overdose crisis by helping to identify service gaps, improving communication within local collaboratives and organizations, and building organizational capacity, utilizing Michigan State University's opioid research and policy experts to aid in these efforts.
</p>

<h2>
  Working With Our Partners
</h2>

<img src="/Content/Images/Overview-OPEN-MSU.jpg" alt="Three VISTA members walking down a street." class="rounded img-fluid float-lg-right mb-4 ml-lg-3">

<p>
  MSU accepts applications from organizations across Michigan to become host sites. Host sites are organizations that supervise VISTA members. These members then collaborate with their host sites by working on a project proposed by the organization during their year of service. This collaboration is beneficial for both the host site and VISTA member, who gains valuable experience in building organizational capacity and project management.
</p>

<p>
  Host sites must be public agencies or 501(c)3 nonprofits, including community health organizations, collaboratives, faith-based organizations, and other organizations working to prevent opioid misuse and overdose.
</p>

<p>
  Because OPEN-MSU is a cost-share partnership, in which all parties contribute to the success and outcomes, host sites must be able to make the required financial contribution. Host sites contribute $5,000 per volunteer, office space and equipment, on-site supervision and logistical support, and position-specific training.
</p>

<p>
  <a href="#" class="btn btn-theme btn-theme-small btn-theme-primary">View All Benefits of Hosting and Serving</a>
</p>

<div class="alert alert-warning mt-5 text-center">
  <p class="p-3 w-75 m-auto">
    OPEN-MSU is always looking for interested host sites and VISTA members who wish to serve with us and our network.
  </p>

<div class="d-flex justify-content-center mb-3">
  <a href="#" class="btn btn-theme btn-theme-small-right btn-theme-tertiary">
    Application Information for Host Sites
  </a>

  &nbsp;&nbsp;&nbsp;

  <a href="vista-member-application" class="btn btn-theme btn-theme-small-left btn-theme-primary">
    Application Information for VISTA Members
  </a>
</div>

</div>
