<?php $page_section = "Programs"; ?>
<?php $page_title = "Mini Grants"; ?>

  
<p>
  ACORN's call for mini-grant submissions takes place annually in February. ACORN issues 2-5 grants for up to $10,000 each.
</p>

<p>
  <a href="#">See previously funded cohorts</a> 
</p>

<hr />

<h2>Topic</h2>

<p>
  Proposals should be 1-2 pages long and include the following: 
</p>

<ul>
  <li>Description of how the proposal focuses on one or more ACORN pillars</li>
  <li>Description of the interdisciplinary nature of the team and how each team member will contribute to the project</li>
  <li>How an intentional partnership with community members will be cultivated within the proposed project</li>
  <li>Plans to incorporate community outreach and dissemination within the proposed project </li>
  <li>Future direction of the proposed project, including the potential to create pilot data for further funding </li>
  <li>How the proposed project aligns with ACORN's mission to inform research, practice, and policy surrounding substance use, addiction, and health</li>
  <li>Proposal should be one page in length but not to exceed two pages in length and submitted via PDF</li>
</ul>


<h2>Eligibility</h2>

<ul>
  <li>Eligibility is limited to teams that include Michigan State University employees as Principal Investigators or Co-Principal Investigators. </li>
  <li>Teams should include faculty from more than one college as Principal Investigators or Co-Investigators</li>
  <li>Priority will be given to teams that include MSU faculty or staff who are already actively engaged in scholarships that are focused on addiction science.</li>
  <li>Teams applying for the funds are encouraged to include a community partner in Michigan as a Co-Principal Investigator or Co-Investigator.</li>
</ul>

<h2>Mini-Grant Processes and Requirements</h2>

<p>
  When preparing your 1-2-page (max) PDF response to this RFP, please adhere to the following requirements and guidelines:
</p>

<ol>
  <li>
    The Responder's proposal should consist of the following sections, in the order listed below:
    
    <ol type="A">
      <li>Title </li>
      
      <li>Identify which of the 5 pillars will be of focus within your project </li>
      
      <li>
        Project description
        
        <ul style="list-style-type:disc;">
          <li>Clearly demonstrate intentional partnership with community members  </li>
          <li>Incorporate community outreach and dissemination </li>
        </ul>
      </li>
      
      <li>
        List of key partners and their roles

        <ul style="list-style-type:disc;">
          <li>MSU faculty or staff required from 2 or more colleges, with priority given to applications with student involvement </li>
        </ul>
      </li>
      
      <li>
        Future direction of project/proposal

        <ul style="list-style-type:disc;">
          <li>Research proposal </li>
          <li>Ensure community engagement</li>
        </ul>
      </li>
      
      <li>Align with <a href="">ACORN's mission</a> to inform research, practice and policy surrounding substance use, addiction, and health</li>
    </ol>

  </li>

  <br />
  
  <li>
    Reporting Requirements:

    <ol type="A">
      <li>
        Present project findings as a PowerPoint presentation to the ACORN Collaborative that describes the following:

        <ul style="list-style-type:disc;">
          <li>Money spent </li>
          <li>Timeline</li>
          <li>Actions completed</li>
          <li>Next steps/future directions</li>
        </ul>
      </li>
      <li>6-month progress presentation to the ACORN Collaborative in November 2024</li>
      <li>Final presentation May 2025</li>
    </ol>

  </li>
</ol>

<hr />

<h2>Contact</h2>
<p>
  The contact person for the purposes of this request is: <br />
  <a href="">Cara Poland</a>, MD, Med
</p>





