<?php $page_section = "Programs"; ?>
<?php $page_title = "Clinical Support"; ?>

  
<h2>Faculty and Staff Support</h2>

<p>
  Lorem ipsum dolor sit amet consectetur adipisicing elit. Quae exercitationem iusto optio asperiores animi deleniti praesentium quibusdam facere provident rerum vero quos sint sunt minus totam, pariatur molestias voluptas a. Lorem ipsum dolor sit amet consectetur adipisicing elit. Animi, voluptatum dolor! Praesentium suscipit, quae cumque ab veritatis minus aut. Aliquid commodi numquam quibusdam ducimus magni eveniet quas nostrum amet doloribus.
</p>

<br />

<hr />

<h2>Student Support</h2>

<h3>MSU's Collegiate Recovery Community (CRC) </h3>
<p>
  CRC supports Spartans in seeking recovery by providing dedicated recovery spaces on campus, raising the visibility of recovery while reducing the stigma associated with addiction, and developing recovery-oriented initiatives to meet identified campus-community needs. Supports for students in recovery include individualized recovery and goal planning, weekly on-campus recovery meetings, 24-hour access to the CRC Student Lounge, wellness and life skills workshops, community service opportunities, and organized sober activities. Recovery Housing is available on campus to offer a safe and supportive living environment where students in recovery can have an authentic college experience and form meaningful relationships based on sobriety, fellowship, and academic success.
</p>

<p>
  <a href="https://healthpromotion.msu.edu/recovery/" target="_blank">Learn more about the Collegiate Recovery Community</a>
</p>
