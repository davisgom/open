<h1>
  VISTA Member Application
</h1>

<div class="row">
  <div class="col-md-7 col-lg-8">
    <h2>
      Eligibility
    </h2>

    <p>
      In order to be eligible to become a VISTA member, applicants must be:
    </p>

    <ul>
    	<li>18 years of age or older.</li>
    	<li>A US citizen, US National, Lawful Permanent Resident (i.e., Green Card status), or person&nbsp;legally residing within a state.</li>
    </ul>

    <p>
      No prior service experience is necessary,&nbsp;but preference will be given to individuals who have experience working on opioid treatment, prevention, or education.
    </p>

    <h2>
      Instructions
    </h2>

    <p>
      Send cover letters and resumes to Rose Henderson&nbsp;(<a href="mailto:hende286@msu.edu" <?php echo $email ?>>hende286@msu.edu</a>). In your cover letter, please indicate your which position you are applying to.&nbsp;If you would like to be considered for more than one location, please list them in order of preference.
    </p>

    <p>
      Visit the <a href="#">Service Descriptions</a> page for more information regarding available projects.
    </p>
  </div>

  <div class="col-md-5 col-lg-4 alert alert-warning">
    <h3 class="h4">
      Important dates:
    </h3>

    <ul>
	<li>Member Applications Due:<b>&nbsp;<br>
	June 15, 2022</b></li>
	<li>Member Candidate Interviews:<br>
	<b>June&nbsp;13, 2022 to July 7, 2022</b></li>
	<li>Notification of acceptance to members:<br>
	<strong>July 8, 2022 OR July 22, 2022</strong></li>
	<li>Member Start Date:<br>
	<b>August 1, 2022 OR August 15, 2022</b></li>
	<li>Member and Supervisor Orientation:<br>
	<b>September&nbsp;2022</b></li>
</ul>
  </div>
</div>
