<section class="intro">
  <div class="intro-heading d-flex align-items-end">
    <div class="container">
      <div class="row">
        <div class="col-12 col-lg-6">
          <h1>
            A Hub for Improving Lives of Those Impacted by Substance Use
          </h1>
        </div>
      </div>
    </div>
  </div>


  <div class="container">
    <div class="row">     
      <div class="intro-text col-12 col-lg-10">
        <p>
          By bringing together MSU faculty, students, volunteers, and staff working on the topic of addiction, the Addiction Consortium on Research & Education Network (ACORN) seeks to change the lives of those impacted by substance use through our collective expertise in engagement, advocacy, public policy, research, patient care, and education.
        </p>

        <p>
          <a href="about" class="btn-theme-moss">
            Learn more
          </a>
        </p>
      </div>       

    </div>
  </div>
</section>


<section class="promo">
  <div class="container">
    <div class="row">
      <div class="col">
        <div class="card">
          <div class="row">
            <div class="col-12 col-md-4 promo-image">
                  <img src="Content/Images/istockphoto-640288152-1024x1024.jpg" class="img-fluid" />
                </div>
      
                <div class="col-12 col-md-8 promo-content align-self-center">
                  <h2>ACORN 2024 Symposium</h2>
                  <p><strong>December 13, 2024</strong> | <strong>Kellogg Center</strong></p>
                  <p>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. In perspiciatis accusantium iure, expedita nostrum totam nisi et, nesciunt dignissimos iusto. Molestiae non in animi nulla illo, accusantium laudantium suscipit.
                  </p>
                  <a href="#" class="btn-theme-outline-white">Learn More</a>
                </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="quick-links">
  <div class="container p-md-0">
    <div class="row row-cols-1 row-cols-md-2 row-cols-lg-4 g-4">

      <div class="col">
        <div class="card h-100">
          <img src="Content/Images/acorn-support.jpg" class="card-img-top" alt="" />
          <div class="quick-link card-body">
            <h2 class="h5">
              CLINICAL SUPPORT
            </h2>
    
            <p>
              Creating an innovative, comprehensive addiction treatment service network 
            </p>

            <a href="clinical-support" class="btn-theme-outline-moss btn-theme-small-left">Learn More</a>
          </div>
        </div>
      </div>

      <div class="col">
        <div class="card h-100">
          <img src="Content/Images/acorn-community.jpg" class="card-img-top" alt="" />
          <div class="quick-link card-body">
            <h2 class="h5">
              COMMUNITY ENGAGEMENT <span class="fs-6 fw-semibold">AND</span> OUTREACH
            </h2>
            
            <p>
              Supporting and growing community engagement and outreach
            </p>

            <a href="capacity-building" class="btn-theme-outline-moss btn-theme-small-left">Learn More</a>
          </div>
        </div>
      </div>

      <div class="col">
        <div class="card h-100">
          <img src="Content/Images/acorn-education.jpg" class="card-img-top" alt="" />
          <div class="quick-link card-body">
            <h2 class="h5">
              EDUCATION
            </h2>
            
            <p>
              Providing anti-stigma teaching and training
            </p>

            <a href="#" class="btn-theme-outline-moss btn-theme-small-left">Learn More</a>
          </div>
        </div>
      </div>

      <div class="col">
        <div class="card h-100">
          <img src="Content/Images/acorn-research.jpg" class="card-img-top" alt="" />
          <div class="quick-link card-body">
            <h2 class="h5">
              RESEARCH
            </h2>
            <p>
              Centering around a health equity framework in addiction science
            </p>

            <a href="#" class="btn-theme-outline-moss btn-theme-small-left align-text-bottom">Learn More</a>
          </div>
        </div>
      </div>
      </div>
    </div>
  </div>
</section>

<div style="height: 60px;"></div>