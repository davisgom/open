<?php $page_section = "Programs"; ?>
<?php $page_title = "Capacity Building"; ?>

  
<h2>Opioid Prevention and Education Network (OPEN-MSU)</h2>

<p>
One of ACORN's goals is to build the capacity of local efforts to create or expand opioid prevention, intervention, and treatment opportunities in low-income Michigan communities. Through the Opioid Prevention and Education Network (OPEN-MSU), a program funded by AmeriCorps, we do this by providing nonprofit, faith-based, and public organizations and agencies with volunteers who serve as AmeriCorps VISTA (Volunteers in Service to America). Through this national service program, VISTA members serve with organizations for one year on projects designed to alleviate poverty.
</p>

<p>
OPEN-MSU supports local efforts to address the opioid overdose crisis by helping to identify service gaps, improving communication within local collaboratives and organizations, and building organizational capacity, utilizing Michigan State University's opioid research and policy experts to aid in these efforts.
</p>

<p>
  <a href="open-msu">Learn more and find applications </a>
</p>

<br />

<hr />

<h2>Community Assessment Support</h2>

<p>
  Lorem ipsum dolor sit amet consectetur adipisicing elit. Quae exercitationem iusto optio asperiores animi deleniti praesentium quibusdam facere provident rerum vero quos sint sunt minus totam, pariatur molestias voluptas a. Lorem ipsum dolor sit amet consectetur adipisicing elit. Animi, voluptatum dolor! Praesentium suscipit, quae cumque ab veritatis minus aut. Aliquid commodi numquam quibusdam ducimus magni eveniet quas nostrum amet doloribus.
</p>

