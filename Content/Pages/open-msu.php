
<?php $page_section = "Programs | Capacity Building"; ?>
<?php $page_title = "Opioid Prevention and Education Network"; ?>

<p>
One of ACORN's goals is to build the capacity of local efforts to create or expand opioid prevention, intervention, and treatment opportunities in low-income Michigan communities. Through the Opioid Prevention and Education Network (OPEN-MSU), a program funded by AmeriCorps, we do this by providing nonprofit, faith-based, and public organizations and agencies with volunteers who serve as <a href="https://www.nationalservice.gov/programs/americorps/americorps-programs/americorps-vista" target="_blank">AmeriCorps VISTA</a> (Volunteers in Service to America). Through this national service program, VISTA members serve with organizations for one year on projects designed to alleviate poverty.
</p>

<p>
OPEN-MSU supports local efforts to address the opioid overdose crisis by helping to identify service gaps, improving communication within local collaboratives and organizations, and building organizational capacity, utilizing Michigan State University's opioid research and policy experts to aid in these efforts.
</p>

<br />

<h4>Working With Our Partners</h4>
<p>
  MSU accepts applications from organizations across Michigan to become host sites. Host sites are organizations that supervise VISTA members. These members then collaborate with their host sites by working on a project proposed by the organization during their year of service. This collaboration is beneficial for the host site and for the VISTA member, who gains valuable experience in building organizational capacity and project management.
</p>

<p>
  Host sites must be public agencies or 501(c)3 nonprofits, including community health organizations, collaboratives, faith-based organizations, and other organizations working to prevent opioid misuse and overdose.
</p>

<p>
  Because OPEN-MSU is a cost-share partnership, in which all parties contribute to the success and outcomes, host sites must be able to make the required financial contribution. Host sites contribute $5,000 per volunteer, office space and equipment, on-site supervision and logistical support, and position-specific training.
</p>

<p>
  <a href="#">View Benefits of hosting and serving</a>
</p>

<br />

<h4>Applications</h4>
<p>
  OPEN-MSU is always looking for interested host sites and VISTA members who wish to serve with us and our network. Applications are accepted on a rolling basis and are processed as received. Please <a href="https://vistaopen.msu.edu/contact">contact us</a> prior to completing the application so we may help you with the process.
</p>

<ul>
<li><a href="https://vistaopen.msu.edu/apply/host-site-application" target="_blank">Application Information for Host Sites</a></li>
<li><a href="https://vistaopen.msu.edu/apply/vista-member-application" target="_blank">Application Information for VISTA Members</a></li>
</ul>


